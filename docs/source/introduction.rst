############
Introduction
############

*******
General
*******
ETCARS is an open source telemetry implementation for American Truck Simulator and Euro Truck Simulator 2 created by `SCS Software <https://scssoft.com>`_. 
ETCARS stands for Electronic Truck Communications Addressing and Reporting System. It is built using c++ and officially compatible with Linux and Windows, with the potential to be built on Mac OS X.
ETCARS runs a local TCP Socket Server on the computer running the game. This allows for data to be broadcast to any clients that wish to connect. 
Starting with version 0.14, ETCARS will only have 64-bit support. This is due to the issues encountered when trying to build for 32-bit platforms.

**********
Built With
**********
ETCARS is built with several other libraries:

- `Boost C++ 1.66 <http://www.boost.org/>`_
- `CryptoPP(Crypto++) <https://www.cryptopp.com/>`_
- `Rapidjson <http://rapidjson.org/>`_
- `OpenSSL <https://www.openssl.org/>`_
- `cURL <https://curl.haxx.se>`_
- `Steamworks <https://partner.steamgames.com>`_
- `SCS SDK <http://modding.scssoft.com/wiki/Documentation/Engine/SDK/Telemetry>`_

************
Contributors
************
Contributors can be found on the `Gitlab <https://gitlab.com/jammerxd/ETCARS>`_ repository.

*******
License
*******
This software is licensed under the `GNU GPL v3.0 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_ License.