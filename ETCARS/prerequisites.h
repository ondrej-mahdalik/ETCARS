#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <boost/asio.hpp>
#ifdef _WIN32
#define _WIN32_WINNT 0x0601
#endif
#define VERSION_SAFE_STEAM_API_INTERFACES
#if defined(__linux__) || defined(__APPLE__)
#include <unistd.h>
#endif
#include <steam/steam_api.h>
#include <ctime>
#include <cstring>
#include <chrono>
#include <time.h>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <iostream>
#include <thread>
#include <boost/thread.hpp>
#include <algorithm>
#include <cstdlib>
#include <deque>
#include <list>
#include <set>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <stdint.h>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "boost/date_time/posix_time/posix_time_types.hpp"
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/error/en.h"
#include <queue>
#include <fstream>
#include <stdexcept>
#include <math.h>
#include <random>
#include <climits>
#ifdef __linux__
#define CURL_STATICLIB
#endif
#include <curl/curl.h>


#include "default.h"
#include "osrng.h"
#include "cryptlib.h"
#include "hex.h"
#include "filters.h"
#include "aes.h"
#include "modes.h"
#include "base64.h"
#include <sys/stat.h>

#include "scssdk_telemetry.h"
#include "eurotrucks2/scssdk_eut2.h"
#include "eurotrucks2/scssdk_telemetry_eut2.h"
#include "amtrucks/scssdk_ats.h"
#include "amtrucks/scssdk_telemetry_ats.h"



#define MAX_WHEEL_COUNT 20

enum class LogLevel : int
{
	INFO = 0,
	WARNING = 1,
	FATAL = 2,
	DBG = 3
};
enum class LogTo :int
{
	GAME = 0,
	FILE = 1,
	BOTH = 2
};

#include "Placement.h"
#include "Debugger.h"
#include "chat_message.h"
#include "TransportLocation.h"
#include "Job.h"
#include "Telemetry.h"


namespace Debugger
{
	extern void init(scs_log_t log, const char* logFileName);
	extern void log(LogTo log, LogLevel level, const char* message);
	extern void log_line(const char* message);
	extern void finish_log();
	extern const char* currentTime();
	extern const char* logLevelToString(LogLevel level);
#ifdef _WIN32
	extern scs_log_t game_log ;
	extern  FILE *log_file ;
	extern  time_t now ;
	extern  tm localTime;
	extern  char timeBuffer[4096];
	extern  char* timeNBuffer ;
	extern  std::string _logLine;
#endif
}


class Placement;
class chat_message;
class TransportLocation;
class Job;
class StorageJob;
class Telemetry;
class API;
#include "API.h"


#define registerChannel(name, type, to) version_params->register_for_channel(SCS_TELEMETRY_##name, SCS_U32_NIL, SCS_VALUE_TYPE_##type, SCS_TELEMETRY_CHANNEL_FLAG_no_value, telemetry_store_##type, &( to ));
#define registerSpecificChannel(name, type, handler, to) version_params->register_for_channel(SCS_TELEMETRY_##name, SCS_U32_NIL, SCS_VALUE_TYPE_##type, SCS_TELEMETRY_CHANNEL_FLAG_no_value, handler, &( to ));
#define UNUSED(x)
SCSAPI_VOID telemetry_store_string(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_RESULT scs_telemetry_init(const scs_u32_t version, const scs_telemetry_init_params_t *const params);
SCSAPI_VOID scs_telemetry_shutdown(void);
SCSAPI_VOID telemetry_store_fplacement(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_VOID telemetry_store_fvector(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_VOID telemetry_store_trailerWorldPlacement(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_VOID telemetry_store_worldPlacement(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_VOID telemetry_store_bool(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_VOID telemetry_store_u64(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_VOID telemetry_store_u32(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_VOID telemetry_store_s32(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_VOID telemetry_store_float(const scs_string_t name, const scs_u32_t index, const scs_value_t *const value, const scs_context_t context);
SCSAPI_VOID telemetry_pause(const scs_event_t event, const void *const UNUSED(event_info), const scs_context_t UNUSED(context));
SCSAPI_VOID telemetry_configuration(const scs_event_t event, const void *const event_info, const scs_context_t UNUSED(context));
SCSAPI_VOID telemetry_frame_end(const scs_event_t UNUSED(event), const void *const UNUSED(event_info), const scs_context_t UNUSED(context));
SCSAPI_VOID telemetry_frame_start(const scs_event_t UNUSED(event), const void *const event_info, const scs_context_t UNUSED(context));
bool changeInDamages(float old[5], float next[5]);
bool isMP();
std::string boolToString(bool bol);
std::string generateTelemetryJSON(const char* status);
void TelemetryWorking();
void SendStatusMessage(const char* status, bool ignoreShutdown=false);
bool findSuffix(std::string &str, std::string &pattern);

void SendSpeedMessage(float speed, float limit, float x, float y, float z, float tx=0.00, float ty=0.00, float tz=0.00);
void SendCollisionMessage(float speed, float limit, float x, float y, float z, float cabinDamage, float chassisDamage, float engineDamage, float transmissionDamage, float tx=0.00, float ty=0.00, float tz=0.00, float trailerDamage=0.00);
void SendTransportationMessage();
void SendLateFeeMessage(unsigned int timeDue, unsigned int time, float x, float y, float z, float tx, float ty, float tz, float fee);
void SendTollMessage();
long long getEpoch();
std::string convertToDayTime(unsigned int minutes);
std::string convertToDateTime(unsigned int minutes);

void doSaveETCARSFile();
void doLoadETCARSFile();
void UploadCompletedDelivery();