##########################################
#Originally written by RootlessAgrarian
#Edited by Josh Menzel
##########################################
WORKDIR="/users/jammerxd/Desktop/Projects"
ETCARS="$WORKDIR/ETCARS/ETCARS"
BOOST="$WORKDIR/Dependencies/boost_1_66_0/build-x64"
SCS_SDK="$WORKDIR/Dependencies/scs_sdk_1_9"
STEAMWK="$WORKDIR/Dependencies/sdk_135"
RAPIDJ="$WORKDIR/Dependencies/rapidjson"
CRYPTOPP="$WORKDIR/Dependencies/cryptopp610"
OPENSSL="$WORKDIR/Dependencies/openssl-1.1.1-pre4/build-x64"
#PATH TO CURL INSTALL DIRECTORY
#THIS DIRECTORY SHOULD HAVE: include and lib folders within
CURL="$WORKDIR/Dependencies/curl-7.59.0/build-x64"

if [ -f "$ETCARS/ETCARSx64.so" ]; then
rm "$ETCARS/ETCARSx64.so"
fi


#64-bit
g++ -o ETCARSx64.so -fPIC -Wall --shared -Wl,-install_name,ETCARSx64.so \
-I$SCS_SDK/include \
-I$SCS_SDK/include/common \
-I$SCS_SDK/include/amtrucks \
-I$SCS_SDK/include/eurotrucks2 \
-I/usr/local/include \
-I$CRYPTOPP \
-I/usr/include \
-I$RAPIDJ/include \
-I$BOOST/include \
-I$CRYPTO/ \
-I$STEAMWK/public \
-I$OPENSSL/include \
-I$CURL/include \
*.cpp \
-DCURL_STATICLIB \
$CURL/lib/libcurl.a \
$OPENSSL/lib/libssl.a \
$OPENSSL/lib/libcrypto.a \
-L/usr/local/lib \
-L$STEAMWK/redistributable_bin/osx32 \
-lpthread \
-L$BOOST/lib \
-lboost_thread-xgcc42-mt-s-x64-1_66 \
-lboost_system-xgcc42-mt-s-x64-1_66 \
-lboost_regex-xgcc42-mt-s-x64-1_66 \
-lboost_iostreams-xgcc42-mt-s-x64-1_66 \
-lsteam_api \
-L$CRYPTOPP/build-x64 \
-lcryptopp \
-pthread -std=c++11 -m64

#NEED TO KNOW WHAT DIRECTORY @loader_path resolves to....
#CHANGE THE SEARCH PATH FOR LIBSTEAM_API.dylib...It resides in ../Frameworks relative to the eurotrucks2 binary.
#install_name_tool -change @loader_path/libsteam_api.dylib @loader_path/../Frameworks/libsteam_api.dylib ETCARSx64.so
#2nd Attempt:
install_name_tool -change @loader_path/libsteam_api.dylib @loader_path/../../Frameworks/libsteam_api.dylib ETCARSx64.so



#cp ETCARSx64.so "/Users/de/Library/Application Support/Steam/steamapps/common/Euro Truck Simulator 2/Euro Truck Simulator 2.app/Contents/MacOS/plugins"

echo DONE
exit 0 