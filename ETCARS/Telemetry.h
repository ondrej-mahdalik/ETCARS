#pragma once
#include "prerequisites.h"
#include "TransportLocation.h"
class TransportLocation;
class Telemetry
{
private:
	static Telemetry* _telemetry;
public:
	static Telemetry* telemetry();
	Telemetry();


	class PluginVersion;
	class Game;
	class Truck;
	class Job;
	class Navigation;
	class Trailer;

	PluginVersion* pluginVersion;
	Game* game;
	Truck* truck;
	Job* job;
	Navigation* navigation;
	Trailer* trailer;

	scs_timestamp_t lastTimestamp;

};
#pragma region Plugin Version
class Telemetry::PluginVersion
{

public:
	PluginVersion();
	int minorVersion;
	int majorVersion;
	std::string pluginVersionOnlyStr;
};
#pragma endregion

#pragma region Game
class Telemetry::Game
{
public:
	Game();
	bool isMultiplayer;
	bool paused;
	bool isDriving;
	int minorVersion;
	int majorVersion;
	std::string gameID;
	std::string gameName;
	std::string gameVersionStr;
	std::string gameVersionOnlyStr;
	unsigned int nextRestStop;
	unsigned int dateTime;
	float localScale;
	struct tm gameTimeTM;
	time_t gameTime;
	std::string osEnvironment;
	std::string architecture;
	std::vector<std::string> substances;
	boost::mutex substanceLock;
};
#pragma endregion

#pragma region Truck
class Telemetry::Truck
{
public:
	Truck();
	class Warnings;
	class Fuel;
	class Damages;
	class Lights;

	Warnings* warnings;
	Fuel* fuel;
	Damages* damages;
	Lights* lights;

	int gear;
	int gearDisplayed;
	float engineRPM;
	unsigned int retarderBrakeLevel;
	float brakeTemperature;
	float fuelRange;
	float oilPressure;
	float oilTemperature;
	float waterTemperature;
	float batteryVoltage;
	bool wipersOn;

	float speed;
	bool parkingBrake;
	bool motorBrake;

	bool electricsEnabled;
	bool engineEnabled;
	float odometer;
	float cruiseControlSpeed;
	bool trailerConnected;
	float maxEngineRPM;
	unsigned int forwardGearCount;
	unsigned int reverseGearCount;
	unsigned int retarderStepCount;
	bool hasTruck;
	std::string make;
	std::string model;
	std::string makeID;
	std::string modelID;
	std::string shifterType;
	Placement worldPlacement;
	Placement trailerWorldPlacement;
	Placement linearVelocity;
	Placement angularVelocity;

	Placement linearAcceleration;
	Placement angularAcceleration;

	Placement cabinOffset;
	Placement hookPosition;
	Placement headPosition;

	Placement cabinAngularVelocity;
	Placement cabinAngularAcceleration;

	
	float differentialRatio;
	float suspensionDeflections[MAX_WHEEL_COUNT];
	boost::mutex wheelCountLock;
	unsigned int wheelCount;
	bool wheelOnGround[MAX_WHEEL_COUNT];
	unsigned int wheelSubstance[MAX_WHEEL_COUNT];
	float wheelAngularVelocity[MAX_WHEEL_COUNT];
	float wheelLift[MAX_WHEEL_COUNT];
	float wheelLiftOffset[MAX_WHEEL_COUNT];
	Placement wheelPosition[MAX_WHEEL_COUNT];
	bool wheelSteerable[MAX_WHEEL_COUNT];
	bool wheelSimulated[MAX_WHEEL_COUNT];
	float wheelRadius[MAX_WHEEL_COUNT];
	bool wheelPowered[MAX_WHEEL_COUNT];
	bool wheelLiftable[MAX_WHEEL_COUNT];
	float wheelSteering[MAX_WHEEL_COUNT];
	float wheelRotation[MAX_WHEEL_COUNT];

	std::vector<float> forwardRatios;
	std::vector<float> reverseRatios;
	boost::mutex ratioMutex;


	float inputSteering;
	float inputThrottle;
	float inputBrake;
	float inputClutch;
	float effectiveSteering;
	float effectiveThrottle;
	float effectiveBrake;
	float effectiveClutch;
	unsigned int hShifterSlot;

	float brakeAirPressure;


	float adBlue;
	float adBlueConsumptionAverage;
	float dashboardBacklight;
};
#pragma endregion

#pragma region Warnings
class Telemetry::Truck::Warnings
{
public:
	bool batteryVoltage;
	bool airPressure;
	bool airPressureEmergency;
	bool oilPressure;
	bool waterTemperature;
	bool fuelLow;
	bool adBlue;
	Warnings();
};
#pragma endregion

#pragma region Fuel
class Telemetry::Truck::Fuel
{
public:
	float capacity;
	float warningLevel;
	float currentLitres;
	float consumptionAverage;
	Fuel();
};
#pragma endregion

#pragma region Damages
class Telemetry::Truck::Damages
{
public:
	float engine;
	float transmission;
	float cabin;
	float chassis;
	float wheels;
	Damages();
};
#pragma endregion

#pragma region Job
class Telemetry::Job
{
public:

	std::string cargoID;
	std::string cargo;
	float mass;
	std::string destinationCityID;
	std::string destinationCity;
	std::string destinationCompanyID;
	std::string destinationCompany;
	std::string sourceCityID;
	std::string sourceCity;
	std::string sourceCompanyID;
	std::string sourceCompany;
	unsigned int income;
	unsigned int deliveryTime;
	float damage;
	bool onJob;
	bool wasFinished;
	bool isLate;
	int timeRemaining;
	boost::mutex transportLocationLock;
	std::vector<TransportLocation> transportLocations;

	Job();
	~Job();
};
#pragma endregion

#pragma region Navigation
class Telemetry::Navigation
{
public:
	float distance;
	float time;
	float speedLimit;
	float lowestDistance;
	float highestDistance;
	Navigation();
};
#pragma endregion



#pragma region Lights
class Telemetry::Truck::Lights
{
public:
	class Blinker;


	Blinker* leftBlinker;
	Blinker* rightBlinker;
	bool lowBeam;
	bool highBeam;
	int frontAux;
	bool beacon;
	int roofAux;
	bool parking;
	bool brake;
	bool reverse;
	Lights();
};
#pragma endregion

#pragma region Blinkers!
class Telemetry::Truck::Lights:: Blinker
{
public:
	bool isOn;
	bool isEnabled;
	Blinker();

};
#pragma endregion


#pragma region Trailer
class Telemetry::Trailer{
	public:
		Trailer();
		float wheelSuspensionDeflections[MAX_WHEEL_COUNT];
		bool wheelOnGround[MAX_WHEEL_COUNT];
		unsigned int wheelSubstance[MAX_WHEEL_COUNT];
		float wheelAngularVelocity[MAX_WHEEL_COUNT];
		Placement wheelPosition[MAX_WHEEL_COUNT];
		bool wheelSteerable[MAX_WHEEL_COUNT];
		bool wheelSimulated[MAX_WHEEL_COUNT];
		float wheelRadius[MAX_WHEEL_COUNT];
		bool wheelPowered[MAX_WHEEL_COUNT];
		float wheelSteering[MAX_WHEEL_COUNT];
		float wheelRotation[MAX_WHEEL_COUNT];
		boost::mutex wheelCountLock;
		unsigned int wheelCount;
		Placement linearVelocity;
		Placement angularVelocity;
		Placement linearAcceleration;
		Placement angularAcceleration;
		Placement hookPosition;
		std::string id;
		std::string cargoAccessoryId;


};
#pragma endregion
