#include "TransportLocation.h"
TransportLocation::TransportLocation()
{
	this->startCoordinates = Placement();
	this->endCoordinates = Placement();
	this->endLocation = "";
	this->startLocation = "";
	this->fee = 0;
	this->distanceTravelled = 0;
}
TransportLocation::TransportLocation(float startX, float startY, float startZ, std::string startLoc, float endX, float endY, float endZ, std::string endLoc, float fee, float distance,std::string startLocID, std::string endLocID)
{
	this->startCoordinates = Placement(startX, startY, startZ, 0, 0, 0);
	this->endCoordinates = Placement(endX, endY, endZ, 0, 0, 0);
	this->startLocation = startLoc;
	this->endLocation = endLoc;
	this->fee = fee;
	this->distanceTravelled = distance;
    this->startLocID = startLocID;
    this->endLocID = endLocID;
}
TransportLocation::TransportLocation(Placement start, Placement end, std::string startLoc, std::string endLoc, float fee, float distance,std::string startLocID, std::string endLocID)
{
	this->startCoordinates = start;
	this->endCoordinates = end;
	this->startLocation = startLoc;
	this->endLocation = endLoc;
	this->fee = fee;
	this->distanceTravelled = distance;
    this->startLocID = startLocID;
    this->endLocID = endLocID;
}
void TransportLocation::SetStartX(float val)
{
	this->startCoordinates.SetX(val);
}
void TransportLocation::SetStartY(float val)
{
	this->startCoordinates.SetY(val);
}
void TransportLocation::SetStartZ(float val)
{
	this->startCoordinates.SetZ(val);
}
void TransportLocation::SetStartLoc(std::string val)
{
	this->startLocation = val;
}
void TransportLocation::SetStartLocID(std::string locID)
{
	this->startLocID = locID;
}
void TransportLocation::SetEndLocID(std::string locID)
{
	this->endLocID = locID;
}
void TransportLocation::SetEndX(float val)
{
	this->endCoordinates.SetX(val);
}
void TransportLocation::SetEndY(float val)
{
	this->endCoordinates.SetY(val);
}
void TransportLocation::SetEndZ(float val)
{
	this->endCoordinates.SetZ(val);
}
void TransportLocation::SetEndLoc(std::string val)
{
	this->endLocation = val;
}
void TransportLocation::SetFee(float val)
{
	this->fee = val;
}
void TransportLocation::SetDistanceTravelled(float val)
{
	this->distanceTravelled = val;
}
float TransportLocation::GetStartX()
{
	return this->startCoordinates.GetX();
}
float TransportLocation::GetStartY()
{
	return this->startCoordinates.GetY();
}
float TransportLocation::GetStartZ()
{
	return this->startCoordinates.GetZ();
}
float TransportLocation::GetEndX()
{
	return this->endCoordinates.GetX();
}
float TransportLocation::GetEndY()
{
	return this->endCoordinates.GetY();
}
float TransportLocation::GetEndZ()
{
	return this->endCoordinates.GetZ();
}
float TransportLocation::GetFee()
{
	return this->fee;
}
float TransportLocation::GetDistanceTravelled()
{
	return this->distanceTravelled;
}
std::string TransportLocation::GetEndLoc()
{
	return this->endLocation;
}
std::string TransportLocation::GetStartLoc()
{
	return this->startLocation;
}
std::string TransportLocation::GetEndLocID()
{
	return this->endLocID;
}
std::string TransportLocation::GetStartLocID()
{
	return this->startLocID;
}
